# Moteurs de jeux - TP1

#### Question 1

La classe `GLWidget` permet la gestion de l'interface et des interactions avec OpenGL. Elle gère notamment les évènements de souris et les changements apportés (rotation, déplacement...).

Les *vertex shaders* permettent de gérer la position des sommets du maillage. Ils changent le repère du sommet, d'abord dans le repère monde uis dans le repère caméra.

Les *fragment shaders* permettent de gérer la couleur des sommets à partir d'un calcul d'angle par rapport à une lumière (position), en utilisant une couleur donnée.  

#### Question 2

La fonction `paintGL` effectue les rotations correspondant aux valeurs des *sliders* sur la matrice du repère monde, puis dessine les triangles obtenus.  

Les mécanismes de transmission d'informations vers l'applications sont les signaux. On émet un signal lorsqu'un changement est effectué, après avoir connecté le *widget* à l'application en associant ce signal à un `slot`. La connexion se fait avec la fonction `connect`, à laquelle on passe en paramètre le *widget* émetteur, le signal, le *widget* receveur et le `slot` concerné.  L'émission de signal se fait avec `emit`.

#### Question 3

#### Question 4
