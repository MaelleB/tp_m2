#include "plane.h"

/*
 * width = size in x
 * length = size in z
 * origin = origin of the plane
 * offset[0] = space between two points in x, offset[1] in z
*/
Plane::Plane(unsigned int width = 1, unsigned int length = 1, QVector3D origin = {0, 0, 0}, std::array<double, 2> offset = {0.05, 0.05}) : m_count(0){

    // Normal of the plane
    QVector3D normal(0, 1, 0);

    // Adds width * length vertices
    for(unsigned int i = 0; i < width; ++i) {
        int x = origin.x() + i * offset[0];
        for(unsigned int j = 0; j < length; ++j) {
            int z = origin.z() + j * offset[1];
            add(QVector3D(x, 0, z), normal);
        }
    }
}

// Adds a vertex to the plane
void Plane::add(const QVector3D &v, const QVector3D &n) {
    // Adds coordinates
    m_data.append(v.x());
    m_data.append(v.y());
    m_data.append(v.z());

    // Adds normals
    m_data.append(n.x());
    m_data.append(n.y());
    m_data.append(n.z());

    // Updates counts
    m_count += 6;
}
