#ifndef TRANSFORM_H
#define TRANSFORM_H

#include <QMatrix3x3>
#include <QMatrix4x4>
#include <QVector3D>

class Transform
{

private:
    QMatrix4x4 _m;
public:
    Transform();
    Transform(const QMatrix4x4& m);

    QMatrix4x4 m() {return _m;}

    QVector3D apply(const QVector3D& v);
    QVector4D apply(const QVector4D& v);

    static Transform scale(float factor);
    static Transform rotate(float angleX, float angleY, float angleZ);
    static Transform translate(QVector3D translation);
};

#endif // TRANSFORM_H
