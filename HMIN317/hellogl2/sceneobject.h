#ifndef SCENEOBJECT_H
#define SCENEOBJECT_H

#include "transform.h"
#include "mesh.h"

#include <vector>

class SceneObject {
private:
    Mesh m_mesh;
    SceneObject* m_parent = nullptr;
    std::vector<SceneObject*> m_children;
    Transform _t;

public:
    SceneObject();
    SceneObject(SceneObject* parent);
    bool isDrawable();
    void draw();

    void assignParent(SceneObject* parent);
    void addChild(SceneObject* child);

    void loadModel(const std::string & filename);
    void setBuffers();


};

#endif // SCENEOBJECT_H
