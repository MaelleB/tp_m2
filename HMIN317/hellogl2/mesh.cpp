#include "mesh.h"

#include <QOpenGLFunctions>

Mesh::Mesh() {

}


void Mesh::loadModel(const std::string & filename) {
    loadOFF(filename, m_vertices, m_indices, m_triangles);
}


void Mesh::setBuffers() {
    setVerticesBuffer();
    setNormalsBuffer();
    setIndicesBuffer();
}

void Mesh::setVerticesBuffer() {
    m_verticesBuffer.destroy();
    m_verticesBuffer.create();
    m_verticesBuffer.bind();
    m_verticesBuffer.allocate(m_vertices.constData(), verticesSize());
}

void Mesh::setNormalsBuffer() {
    m_normalsBuffer.destroy();
    m_normalsBuffer.create();
    m_normalsBuffer.bind();
    m_normalsBuffer.allocate(m_normals.constData(), normalsSize());
}

void Mesh::setIndicesBuffer() {
    m_indicesBuffer.destroy();
    m_indicesBuffer.create();
    m_indicesBuffer.bind();
    m_indicesBuffer.allocate(m_indices.constData(), indicesSize());
}


void Mesh::loadIntoVBO() {
    QOpenGLFunctions *f = QOpenGLContext::currentContext()->functions();

    m_verticesBuffer.bind();
    f->glEnableVertexAttribArray(0);
    f->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), nullptr);
    m_verticesBuffer.release();

    m_normalsBuffer.bind();
    f->glEnableVertexAttribArray(1);
    f->glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), nullptr);
    m_normalsBuffer.release();
}

void Mesh::draw() {
#ifdef QT_DEBUG
    if(m_indices.empty()) {
        throw std::logic_error("Indices are empty");
    }
    if(m_vertices.empty()) {
        throw std::logic_error("Vertices are empty");
    }
#endif
    loadIntoVBO();
    m_indicesBuffer.bind();
    glDrawElements(
                GL_TRIANGLES,      // mode
                m_indices.size(),    // count
                GL_UNSIGNED_INT,   // type
                nullptr          // element array buffer offset
                );
    m_indicesBuffer.release();
}

