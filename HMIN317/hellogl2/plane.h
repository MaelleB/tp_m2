#ifndef PLANE_H
#define PLANE_H

#include <qopengl.h>
#include <QVector>
#include <QVector3D>
#include <array>

class Plane
{
public:
    Plane(unsigned int width, unsigned int length, QVector3D origin, std::array<double, 2> offset);
    const GLfloat *constData() const { return m_data.constData();}
    int count() const { return m_count;}
    int vertexCount() const { return m_count/6;}
    void add(const QVector3D &v, const QVector3D &n);
private:
    QVector<GLfloat> m_data;
    int m_count;
};

#endif // PLANE_H
