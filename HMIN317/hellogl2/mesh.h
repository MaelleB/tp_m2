#ifndef MESH_H
#define MESH_H

#include <QVector>
#include <array>
#include <string>
#include <utility>
#include <QVector3D>
#include <QVector2D>
#include <QOpenGLContext>
#include <QOpenGLBuffer>

#include "objloader.h"

class Mesh
{
private:
    QVector< QVector3D > m_vertices;
    QVector< unsigned short > m_indices;
    QVector< std::vector<unsigned short > > m_triangles;
    QVector< QVector3D > m_normals;

    QOpenGLBuffer m_verticesBuffer;
    QOpenGLBuffer m_normalsBuffer;
    QOpenGLBuffer m_indicesBuffer;

public:
    Mesh();
    void loadModel(const std::string & filename);

    void setBuffers();
    void setVerticesBuffer();
    void setNormalsBuffer();
    void setIndicesBuffer();

    void loadIntoVBO();
    void draw();

    inline int verticesSize() const { return static_cast<GLsizeiptr>(sizeof(QVector3D)) * m_vertices.size(); }
    inline int normalsSize() const { return static_cast<GLsizeiptr>(sizeof(QVector3D)) * m_normals.size(); }
    inline int indicesSize() const { return static_cast<GLsizeiptr>(sizeof(unsigned short)) * m_indices.size(); }
};

#endif // MESH_H
