#include "transform.h"

Transform::Transform() {}

Transform::Transform(const QMatrix4x4& m) : _m(m) {}

QVector3D Transform::apply(const QVector3D &v) {
    QVector4D vec(v.toVector4D()); // to vector4D in order to be able to multiply the matrix with the vector
    vec.setW(1); // To ensure translations are applied, W is a direction
    return (_m * vec).toVector3D(); // Now we want a 3D vector as a return
}


QVector4D Transform::apply(const QVector4D &v) {
    return _m * v;
}

Transform Transform::scale(float factor) {
    QMatrix4x4 m;
    m.scale(factor);
    return Transform(m);
}

Transform Transform::rotate(float angleX, float angleY, float angleZ) {
    QMatrix4x4 m;
    m.rotate(angleX, QVector3D(1.0f, 0.0f, 0.0f));
    m.rotate(angleY, QVector3D(0.0f, 1.0f, 0.0f));
    m.rotate(angleZ, QVector3D(0.0f, 0.0f, 1.0f));

    return Transform(m);
}

Transform Transform::translate(QVector3D translation) {
    QMatrix4x4 m;
    m.translate(translation);

    return Transform(m);
}
