#include "sceneobject.h"

SceneObject::SceneObject() {}

SceneObject::SceneObject(SceneObject* parent) : m_parent(parent) {}

void SceneObject::assignParent(SceneObject* parent) {
    m_parent = parent;
}

void SceneObject::addChild(SceneObject* child) {
    m_children.push_back(child);
}

void SceneObject::draw() {
    m_mesh.draw();
}

void SceneObject::loadModel(const std::string &filename) {
    m_mesh.loadModel(filename);
}

void SceneObject::setBuffers() {
    m_mesh.setBuffers();
}

