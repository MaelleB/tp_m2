#ifndef OBJLOADER_H
#define OBJLOADER_H
#include <string>
#include <QVector>
#include <QVector3D>

bool loadOFF( const std::string & filename ,
              QVector< QVector3D > & vertices ,
              QVector< unsigned short > & faces) ;


bool loadOFF( const std::string & filename ,
              QVector< QVector3D > & vertices ,
              QVector< unsigned short > & indices,
              QVector< std::vector<unsigned short > > & triangles) ;
#endif
