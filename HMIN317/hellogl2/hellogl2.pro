HEADERS       = glwidget.h \
                window.h \
                mainwindow.h \
                logo.h \
    plane.h \
    transform.h \
    sceneobject.h \
    objloader.h \
    mesh.h
SOURCES       = glwidget.cpp \
                main.cpp \
                window.cpp \
                mainwindow.cpp \
                logo.cpp \
    plane.cpp \
    transform.cpp \
    sceneobject.cpp \
    objloader.cpp \
    mesh.cpp

QT           += widgets

# install
target.path = $$[QT_INSTALL_EXAMPLES]/opengl/hellogl2
INSTALLS += target
