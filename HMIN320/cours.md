# HMIN320 - Vision, réalité augmentée et réalité virtuelle

### Chapitre 1 - Modèle de caméra

#### Modèle de Gauss

![](gauss_model.jpg)

#### Modèle Sténopée

![](stenopee_model.jpg)


On a $\frac{x}{f} = \frac{X}{Z}$ et $\frac{y}{f} = \frac{Y}{Z}$

#### Coordonnées homogènes

$\vec{x_w} = r_{11}\vec{x_c} + r_{21}\vec{y_c} + r_{31}\vec{z_c}$

$\vec{y_w} = r_{12}\vec{x_c} + r_{22}\vec{y_c} + r_{32}\vec{z_c}$

$\vec{x_w} = r_{13}\vec{x_c} + r_{23}\vec{y_c} + r_{33}\vec{z_c}$


$\vec{O_wP} = X_w\vec{x_w} + Y_w\vec{y_w} + Z_w\vec{z_w}$

$\vec{O_cP} = X_c\vec{x_c} + Y_c\vec{y_c} + Z_c\vec{z_c}$

$\vec{O_wP} = \vec{O_cO_w} + \vec{O_wP} = t_x\vec{x_c} + t_y\vec{y_c} + t_z\vec{z_c} + \vec{O_wP}$

$\vec{O_wP} = (X_wr\{11) + Y_wr_{21} + Z_wr_{31})\vec{x_c} + (X_wr_{12} + Y_wr_{22} + Z_wr_{32})\vec{y_c} + ...$

- Coordonnées non homogènes :

$\begin{bmatrix}
  X_c \\ Y_c \\ Z_c
\end{bmatrix} =
\begin{bmatrix}
  r_{11} & r_{21} & r_{31} \\
  r_{12} & r_{22} & r_{32} \\
  r_{13} & r_{23} & r_{33}
\end{bmatrix}
\begin{bmatrix}
  X_w \\ Y_w \\ Z_w
\end{bmatrix} +
\begin{bmatrix}
  t_x \\ t_y \\ t_z
\end{bmatrix}
\rightarrow
\begin{bmatrix}
  X_c \\ Y_c \\ Z_c
\end{bmatrix} -
\begin{bmatrix}
  t_x \\ t_y \\ t_z
\end{bmatrix} =
\begin{bmatrix}
  r_{11} & r_{21} & r_{31} \\
  r_{12} & r_{22} & r_{32} \\
  r_{13} & r_{23} & r_{33}
\end{bmatrix}
\begin{bmatrix}
  X_c \\ Y_c \\ Z_c
\end{bmatrix}$

$\begin{bmatrix}
  r_{11} & r_{21} & r_{31} \\
  r_{12} & r_{22} & r_{32} \\
  r_{13} & r_{23} & r_{33}
\end{bmatrix}
\begin{bmatrix}
  X_c \\ Y_c \\ Z_c
\end{bmatrix} -
\begin{bmatrix}
  r_{11} & r_{21} & r_{31} \\
  r_{12} & r_{22} & r_{32} \\
  r_{13} & r_{23} & r_{33}
\end{bmatrix}
\begin{bmatrix}
  t_x \\ t_y \\ t_z
\end{bmatrix} =
\begin{bmatrix}
  r_{11} & r_{21} & r_{31} \\
  r_{12} & r_{22} & r_{32} \\
  r_{13} & r_{23} & r_{33}
\end{bmatrix}
\begin{bmatrix}
  X_w \\ Y_w \\ Z_w
\end{bmatrix}$

- Coordonnées homogènes :

$\begin{bmatrix}
  X_c \\ Y_c \\ Z_c \\ 1
\end{bmatrix} =
\begin{bmatrix}
  r_{11} & r_{21} & r_{31} & t_x \\
  r_{12} & r_{22} & r_{32} & t_y \\
  r_{13} & r_{23} & r_{33} & t_z \\
  0 & 0 & 0 & 1
\end{bmatrix}
\begin{bmatrix}
  X_w \\ Y_w \\ Z_w \\ 1
\end{bmatrix}$

Pour passer d'un repère à l'autre : $\begin{bmatrix}
  X_3 \\ Y_3 \\ Z_3
\end{bmatrix} = M^3_2
\begin{bmatrix}
  x_2 \\ y_2 \\ z_2
\end{bmatrix} = M^3_2M^2_1
\begin{bmatrix}
  x_1 \\ y_1 \\ z_1
\end{bmatrix} = M^3_1
\begin{bmatrix}
  x_1\\ y_1 \\ z_1
\end{bmatrix}$


![](schema.jpg)

$\frac{x}{f} = \frac{X_c}{Z_c}  \rightarrow x = f\frac{X_c}{Z_c}$, $\frac{y}{f} = \frac{Y_c}{Z_c}  \rightarrow y = f\frac{Y_c}{Z_c}$

$\left\{{u = \alpha_ux + u_0 \\ v = \alpha_vy + v_0}\right.$

$u = \alpha_uf\frac{X_c}{Z_c} + u_0$ et $v = \alpha_vf\frac{Y_c}{Z_c} + v_0$

Pour avoir un système linéaire :
$\begin{bmatrix}
  U \\ V \\ S
\end{bmatrix}
\begin{bmatrix}
  k_u & 0 & u_0 & 0 \\
  0 & k_v & v_0 & 0 \\
  0 & 0 & 1 & 0
\end{bmatrix}
\begin{bmatrix}
  X_c \\ Y_c \\ Z_c \\ 1
\end{bmatrix}$

Soit
$\begin{bmatrix}
  U \\ V \\ S
\end{bmatrix} =
\underbrace{\begin{bmatrix}
  k_u & 0 & u_0 & 0 \\
  0 & k_v & v_0 & 0 \\
  0 & 0 & 1 & 0
\end{bmatrix}
\begin{bmatrix}
 \\ M^c_w \\  \\
\end{bmatrix}}
_{\begin{bmatrix}
  c_{11} & c_{12} & c_{13} & c_{14} \\
  c_{21} & c_{22} & c_{23} & c_{24} \\
  c_{31} & c_{32} & c_{33} & c_{34}
\end{bmatrix} =
\begin{bmatrix}
  Cc
\end{bmatrix}}
\begin{bmatrix}
  X_c \\ Y_c \\ Z_c \\ 1
\end{bmatrix}$

Ce vecteur reste le même si on multiplie par exemple U, V et S par 10. Ce qui représente un vecteur à un facteur d'échelle près. La matrice est aussi connue à un facteur d'échelle près.

### Chapitre 2 - Stéréovision

#### Principe

Mise en correspondance de deux images $\begin{bmatrix}
  U \\ V \\ S
\end{bmatrix}d'un point de vue différent pour permettre de voir la profondeur. Plus un objet est proche, plus sa projection sur l'oeil droit et l'oeil gauche est différente.

Stéréo = solide.

#### Contrainte épipolaire

$\Delta_2(m1)$ qui est la droite épipolaire sur l'image 2 correspondant au point $m_1$ sur l'image 1.

On doit minimiser la distance par rapport à la droite et par rapport aux points particuliers pour la mise en correspondance.

(Faire schéma)

$E_2$ épipole sur l'image 2. $E_2$ est la projection de $O_1$ sur l'image 2 : $E_2 = \begin{bmatrix}C_2 & c_2\end{bmatrix} O_1$

$E_1 = \begin{bmatrix}C_1 & c_1\end{bmatrix} O_1$ similairement.

Supposons que l'on ait
$\begin{bmatrix}
  x_1 \\
  y_1 \\
  z_1 \\
  1
\end{bmatrix} =
\begin{bmatrix}

\end{bmatrix}$

--__-- compléter le cours --__--

#### Flot optique

##### 1. Principe

Variation d'intensité lumineuse $\leftrightarrow$ mouvement apparent

##### 2. Mise en équation

Equation de contrainte du flot optique :

$I(x + \Delta x, y + \Delta y, t + \Delta t) = I(x,y,t) + \frac{\delta I}{\delta x} (x,y,t).\Delta x$

$+ \frac{\delta I}{\delta y} (x,y,t).\Delta y$

$+ \frac{\delta I}{\delta t} (x,y,t).\Delta t$

$+ O²()$


$\frac{\delta I}{\delta x} (x,y,t).\Delta x + \frac{\delta I}{\delta y} (x,y,t).\Delta y + \frac{\delta I}{\delta t} (x,y,t).\Delta t \approx 0$


$\begin{bmatrix}
  I_x & I_y
\end{bmatrix}
\begin{bmatrix}
  \Delta x \\ \Delta y
\end{bmatrix} = -I_t \Delta t$

##### 3. Régularisation du FO

- Locale : tous les points autour d'un point donné se déplacent de la même façon.

$\begin{bmatrix}
  I_x¹ & I_y¹ \\
  . \\
  . \\
  .
  I_x⁹ & I_y⁹
\end{bmatrix}
\begin{bmatrix}
  \Delta x \\ \Delta y
\end{bmatrix}
= -\Delta t
\begin{bmatrix}
  I_t¹ \\
  . \\
  . \\
  . \\
  I_t⁹
\end{bmatrix}$

- Globale : $zoom + t_x + t_y + rot_z$


$\begin{bmatrix}
  x' \\ y'
\end{bmatrix} =
\begin{bmatrix}
  a & b \\
  -b & a
\end{bmatrix}
\begin{bmatrix}
  x \\ y
\end{bmatrix} +
\begin{bmatrix}
  t_x \\ t_y
\end{bmatrix}$

##### 4. Grand mouvement

Décimation : $\frac{N}{2}, \frac{N}{4}, \frac{N}{8} ...$

##### 5. Camshift

Continously Adaptive Mean Shift.

Mean shift (algorithme de déplacement de la moyenne) : soit un ensemble de points en déplacement, on se rapproche de leur barycentre.

$\bar{x} = \frac{1}{\sum P_k} \sum P_kx_k$

$\bar{y} = \frac{1}{\sum P_k} \sum P_ky_k$

### Chapitre ? Vision en lumière structurée

#### 1. Principe

Le principe de lumière structurée consiste à projeter une séquence de franges sur un objet et à enregistrer à l’aide d’une caméra les images de ces franges déformées par le relief de l’objet.

Le traitement de la séquence d’images fournit, pour chaque pixel de la caméra, la distance du point correspondant de la surface. On obtient alors un nuage de points de très grande densité fidèle à la géométrie 3D de la surface.

#### 2. Projection d'un point

On a un laser, on sait où se trouve la droite formée par le laser dans le repère caméra. On est capable de tracer la droite du centre de projection de la caméra. Le point éclairé se trouve alors à l'intersection entre les deux droites.

Emission spontanée (exemple : infrarouge du corps) vs émission stimulée (projection de photons sur des électrons qui vont projeter des électrons). Exemmple : laser : Light Amplification by Stimulated Emission of Radiation.

#### 3. Projection d'une droite

On projette un rayon laser sur une lentille cylindrique qui transforme le rayon en plan laser. On obtient une ligne sur l'objet.

#### 4. Projection de bandes

#### 5. Projection de motifs codés

#### 6. Forme par occultation

Shape from silhouette

#### 7. Shape from shading (ombrage)

### Chapitre ? Vision omnidirectionnelle

#### 1. Principe

Vidéo à 360°.

#### 2. Assemblage de caméra

#### 3. Catadioptrique

#### 4. Fisheye 
