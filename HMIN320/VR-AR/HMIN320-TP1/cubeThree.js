var renderer = null,
scene = null,
camera = null,
cube = null;
earthOrbitAngle = 0;
earthOrbitSpeed = 0.1;
earthOrbitRadius = 10;

moonOrbitAngle = 0;
moonOrbitRadius = 4;
moonOrbitSpeed = 0.2;

main();

function initSound() {
  // instantiate a listener
  var audioListener = new THREE.AudioListener();
  // add the listener to the camera
  camera.add( audioListener );
  // instantiate audio object
  ambientSound = new THREE.Audio( audioListener );
  // add the audio object to the scene
  scene.add( ambientSound );
  // instantiate a loader
  var loader = new THREE.AudioLoader();
  // load a resource
  loader.load('./sounds/ambient.mp3',
    // Function when resource is loaded
    function ( audioBuffer ) {
    ambientSound.setBuffer( audioBuffer );
    ambientSound.setLoop( true );
    ambientSound.setVolume( 0.5 );
    ambientSound.play();
  });

  document.body.addEventListener('keydown', toggle);
}

function toggle () {
  if ( ambientSound.isPlaying === true ) {
    ambientSound.pause();
  }
  else {
    ambientSound.play();
  }
}

function animate() {
  requestAnimationFrame(animate);
  var angle = .0025;
  earthOrbitAngle -= earthOrbitSpeed;
  earthOrbitRadians = Math.PI * earthOrbitAngle / 180;

  // Updates earth and clouds positions
  earth.position.x = earthOrbitRadius * Math.cos(earthOrbitRadians);
  clouds.position.x = earth.position.x;
  earth.position.z = earthOrbitRadius * Math.sin(earthOrbitRadians);
  clouds.position.z = earth.position.z;

  // Earth rotates on itself
  solarSystem.rotation.y -= angle;

  moonOrbitAngle += moonOrbitSpeed;
  moonOrbitRadians = Math.PI * moonOrbitAngle / 180;
  moon.position.x = moonOrbitRadius * Math.cos(moonOrbitRadians);
  moon.position.z = moonOrbitRadius * Math.sin(moonOrbitRadians);

  renderer.render(scene, camera);
}

function loadTextures(textureURLs) {
  var loaded = 0;
  function loadedOne() {
    loaded++;
    if (loaded == textureURLs.length) {
    for (var i = 0; i < textureURLs; i++)
    textures[i].needsUpdate = true;
    }
  }
  var textures = [];
  for (var i = 0; i < textureURLs.length; i++) {
    var tex = new THREE. TextureLoader().load( textureURLs[i], undefined, loadedOne );
    textures.push(tex);
  }
  return textures;
}

function main() {
  var canvas = document.getElementById("glcanvas");
  // Create the Three.js renderer and attach it to our canvas
  renderer = new THREE.WebGLRenderer( {canvas: canvas, antialias: true} );
  // Set the viewport size
  renderer.setSize(canvas.width, canvas.height);
  document.body.appendChild(renderer.domElement);
  // Create a new Three.js scene
  scene = new THREE.Scene();

  // Add a camera so we can view the scene
  camera = new THREE.PerspectiveCamera(45, canvas.width / canvas.height, 1, 400);
    camera.position.set(0, 0, 30);
    scene.add(camera);

  //Orbit Controls
  var orbit = new THREE.OrbitControls( camera);

  initSound();

  window.addEventListener( 'resize', function () {
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();
  renderer.setSize( window.innerWidth, window.innerHeight );
  }, false );

  var cube = null;
  textureURLs = [ // URLs of the six faces of the cube map
    "./textures/cube/posx.jpg",
    "./textures/cube/negx.jpg",
    "./textures/cube/posy.jpg",
    "./textures/cube/negy.jpg",
    "./textures/cube/posz.jpg",
    "./textures/cube/negz.jpg"
  ];
  // skybox
  var textures = loadTextures(textureURLs);
  var materials = [];
  for (var i = 0; i < 6; i++) {
    materials.push( new THREE.MeshBasicMaterial( {
      color: "white",
      // IMPORTANT: To see the inside of the cube, back faces must be rendered!
      side: THREE.BackSide,
      map: textures[i]
    } ) );
  }
  var cubeGeometry = new THREE.CubeGeometry(200,200, 200);
  cube = new THREE.Mesh(cubeGeometry, new THREE.MeshFaceMaterial(materials));
  scene.add(cube);
  // Add a directional light to show off the object
  var light = new THREE.PointLight( 0xffffff, 3, 0, 0);
  // Position the light out from the scene, pointing at the origin
  light.position.set(0, 0, 0);
  scene.add(light);
  var ambientLight = new THREE.AmbientLight( 0xcccccc, 0.4 );
  scene.add(ambientLight);

  // Imported mesh
    var mtlLoader = new THREE.MTLLoader();
    mtlLoader.setResourcePath('./models/snowman/');
    mtlLoader.setPath('./models/snowman/');
    mtlLoader.load('snowman.mtl', function (materials) {
        materials.preload();
        objLoader = new THREE.OBJLoader();
        objLoader.setMaterials(materials);
        objLoader.setPath('./models/snowman/');
        objLoader.load('snowman.obj', function (object) {
            importMesh = object;
            //object.rotation.x = -Math.PI/2;
            //object.scale.set(10,10,10);
            importMesh.rotation.set(-Math.PI / 2, 0, 0);
            importMesh.position.set(0, 5, 0);
            importMesh.scale.set(0.2, 0.2, 0.2);
            scene.add(importMesh);
        },
            // called when loading is in progresses
            function (xhr) {
                console.log((xhr.loaded / xhr.total * 100) + '% loaded');
            },
            // called when loading has errors
            function (error) {
                console.log('An error happened');
            }
        )
    });

  var sphereGeometry2 = new THREE.SphereGeometry(3,32,32);
  var sunMaterial = new THREE.MeshBasicMaterial({
    map: new THREE. TextureLoader().load ("./textures/sun2.jpg"),
    color: 0xffffff,
    specular: 0x000000,
    shininess: 25,
    castShadow:false
  });
  sun = new THREE.Mesh(sphereGeometry2, sunMaterial);
  sun.position.set(0,0,0);
  sun.rotation.x = Math.PI / 5;
  sun.rotation.y = Math.PI / 5;
  //scene.add(sun);

  // Creates a sphere
  var sphereGeometry = new THREE.SphereGeometry(2,32,32);
  var sphereMaterial = new THREE.MeshPhongMaterial({color: 0xcd0000});
  var earthMaterial = new THREE.MeshPhongMaterial({
    map: new THREE. TextureLoader().load ("./textures/no_clouds.png"),
    color: 0xaaaaaa,
    specular: 0x000111,
    shininess: 25
  });

  earth = new THREE.Mesh(sphereGeometry, earthMaterial);
  earth.position.set(8,0,1);
  earth.rotation.x = Math.PI / 5;
  earth.rotation.y = Math.PI / 5;
  //sun.add(earth);

  // Clouds
  var cloudGeometry = new THREE.SphereGeometry(2.05, 20, 20);
  var cloudMaterial = new THREE.MeshPhongMaterial({
    map: new THREE. TextureLoader().load ("./textures/fair_clouds.png"),
    transparent: true,
    opacity: 0.8
  });
  clouds = new THREE.Mesh(cloudGeometry, cloudMaterial);
  // position the clouds
  clouds.position.x = 8;
  clouds.position.z = 1;
  //sun.add(clouds);

  var moonGeometry = new THREE.SphereGeometry(0.5, 32, 32);
  var moonMaterial = new THREE.MeshPhongMaterial({
    map: new THREE. TextureLoader().load ("./textures/moon.jpeg"),
    color: 0xffffff,
    specular: 0xffffff,
    shininess: 10
  });
  moon = new THREE.Mesh(moonGeometry, moonMaterial);
  moon.position.x = 3;
  moon.position.z = 1;
  //earth.add(moon);

  solarSystem = new THREE.Group();
  solarSystem.add(sun);
  solarSystem.add(earth);
  solarSystem.add(clouds);
  //scene.add(solarSystem);


  // Run the run loop
  animate();
};
