#include "ImageBase.h"
#include <stdio.h>
#include <time.h>
#include <math.h>
#include <stdlib.h>
#include <iostream>

using namespace std;

void taille(int* t) {
	ImageBase imIn;
	imIn.load("perroquet.ppm"); //mettre le nom de l'image
	ImageBase *R = imIn.getPlan(ImageBase::PLAN_R);
	ImageBase *G = imIn.getPlan(ImageBase::PLAN_G);
	ImageBase *B = imIn.getPlan(ImageBase::PLAN_B);
	int n = imIn.getHeight();
	int m = imIn.getWidth();
	t[0] = n;
	t[1] = m;

}
void conversion(double Y[512][512], double Cr[512][512], double Cb[512][512]) {
	ImageBase imIn;
	imIn.load("perroquet.ppm"); //mettre le nom de l'image
	ImageBase *R = imIn.getPlan(ImageBase::PLAN_R);
	ImageBase *G = imIn.getPlan(ImageBase::PLAN_G);
	ImageBase *B = imIn.getPlan(ImageBase::PLAN_B);
	int n = imIn.getHeight();
	int m = imIn.getWidth();

	for(int y = 0; y < m; ++y) {
		for(int x = 0; x < n; ++x) {
			Y[y][x] = 0.299 * (*R)[y][x] + 0.587 * (*G)[y][x] + 0.114 * (*B)[y][x];
			Cr[y][x] = 0.5 * (*R)[y][x] -0.4187 * (*R)[y][x] -0.0813 * (*B)[y][x] + 128;
			Cb[y][x] = -0.1687 * (*R)[y][x] -0.3313 * (*R)[y][x] + 0.5 * (*B)[y][x] + 128;
		}
	}

	delete R;
	delete G;
	delete B;
}

int Quantification(int f, int d) {// d = M - N + 1
	if(f == 1) {// Frequence = BF
		return f;
	}
	else if(f==2) { // HFH et HFV
		return 140 / d;
	}
	else { // THF
		return 240 / d;
	}
}

void Tondelette(double Y[512][512], double Cr[512][512], double Cb[512][512], double resY[512][512], double resCr[512][512], double resCb[512][512], int* taille, int N, int M) {// N le nombre de décomposition qui va décroitre à chaque appel, M le nombre de décomposition initial
	//on doit traiter que le cube en haut à droite seulement, on divise donc la largeur et la longueur de l'image considérée en fonction de N par rapport à M, le reste est statique
	int t2 = taille[1]/(2^(M - N));
	int t1 = taille[0]/(2^(M - N));
	int i, j;
	int Q;
	for(int y = 0; y < t2; ++y) {
		for(int x = 0; x < t1; ++x) {
			if(x > t1/2) {// HFH ou THF
				if(y > t2/2) {//THF
					i = (x - t1/2 ) * 2 ;
					j = (y - t2/2 ) * 2 ;
					Q = Quantification(3, M - N +1);
					//K - L, K = A - B, L = C - D
					resY[y][x] = (Y[j][i] - Y[j][i+1]) - (Y[j+1][i] - Y[j+1][i+1]);
					resCr[y][x] = (Cr[j][i] - Cr[j][i+1]) - (Cr[j+1][i] - Cr[j+1][i+1]);
					resCb[y][x] = (Cb[j][i] - Cb[j][i+1]) - (Cb[j+1][i] - Cb[j+1][i+1]);
				}
				else { //HFH
					i = (x - t1/2 ) * 2 ;
					j = y * 2 ;
					Q = Quantification(2, M - N +1);
					// X - Y avec X = A + B/2, Y = C + D/2
					resY[y][x] = (Y[j][i] + Y[j][i+1])/2 - (Y[j+1][i] + Y[j+1][i+1])/2;
					resCr[y][x] = (Cr[j][i] + Cr[j][i+1])/2 - (Cr[j+1][i] + Cr[j+1][i+1])/2;
					resCb[y][x] = (Cb[j][i] + Cb[j][i+1])/2 - (Cb[j+1][i] + Cb[j+1][i+1])/2;
				}
			}
			else {// BF ou HFV
				if(y > t2/2) {//HFV
					i = (x) * 2 ;
					j = (y - t2/2 ) * 2 ;
					Q = Quantification(2, M - N +1);
					// K + L/2 avec K = A - B, L = C - D
					resY[y][x] = ((Y[j][i] - Y[j][i+1]) + (Y[j+1][i] - Y[j+1][i+1]))/2;
					resCr[y][x] = ((Cr[j][i] - Cr[j][i+1]) + (Cr[j+1][i] - Cr[j+1][i+1]))/2;
					resCb[y][x] = ((Cb[j][i] - Cb[j][i+1]) + (Cb[j+1][i] - Cb[j+1][i+1]))/2;
				}
				else {//BF
					i = (x) * 2 ;
					j = (y) * 2 ;
					Q = Quantification(1, M - N +1);
					//X + Y/2 avec X = A + B /2 et Y = C + D/2
					resY[y][x] = ((Y[j][i] + Y[j][i+1])/2 + (Y[j+1][i] + Y[j+1][i+1])/2)/2;
					resCr[y][x] = ((Cr[j][i] + Cr[j][i+1])/2 + (Cr[j+1][i] + Cr[j+1][i+1])/2)/2;
					resCb[y][x] = ((Cb[j][i] + Cb[j][i+1])/2 + (Cb[j+1][i] + Cb[j+1][i+1])/2)/2;
				}

			}
			resY[y][x] = resY[y][x] / Q; // Vérifier qu'il n'y a pas de négatifs et que tout soit entier
			resCr[y][x] = resCr[y][x] / Q;
			resCb[y][x] = resCb[y][x] / Q;

		}
	}

	for(int y = 0; y < t2; ++y) {
		for(int x = 0; x < t1; ++x) {
			Y[y][x] = resY[y][x];
			Cr[y][x] = resCr[y][x];
			Cb[y][x] = resCb[y][x];
		}
	}
	if(N > 1) {
		Tondelette(Y, Cr, Cb, resY, resCr, resCb, taille, N-1, M);
	}
}

void echange(float* f, int i, int j) {
	float m = f[i];
	f[i] = f[j];
	f[j] = m;
}

int PlusPetit(float* f, int fin, int interdit) {
	int pp = 0;
	float vpp = f[pp];

	if(interdit == 0) {
		pp = 1;
		vpp = f[pp];
	}

	for(int i = 0; i <= fin; i++) {
		if(f[i] < vpp && i != interdit) {
			pp = i;
			vpp = f[i];
		}
	}
	return pp;
}

void Huffman(float* f, float* bit, int l) { // l défini la fréquence prise en compte
	int fin = 254;
	int debut = 0;
	while(debut < fin) {
		if(f[debut] == 0) {
			echange(f, debut, fin);
			fin--;
		}
		else {
			debut++;
		}
	}// on a plus que les frequences strictement positives entre 0 et fin

	float prof[255];
	for(unsigned int i = 0; i < 255; ++i) {
		prof[i] = 0;
	}
	int pp1, pp2;
	while(fin > 0) {
		pp1 = PlusPetit(f, fin, 255);
		pp2 = PlusPetit(f, fin, pp1);
		echange(f, fin, pp2);
		echange(f, fin - 1, pp1);
		echange(prof, fin, pp2);
		echange(prof, fin - 1, pp1);
		fin--;
		f[fin] = f[fin] + f[fin + 1];
		prof[fin] += 1;
	}
	bit[l] = prof[0];
}

void HuffmanPrep(double Y[512][512], double Cr[512][512], double Cb[512][512], int* taille, float* bit) { // bit contient le nombre de bits nécessaires
	int occY[255], occCr[255], occCb[255];
	for(unsigned int i = 0; i < 255; ++i) {
		occY[i] = 0;
		occCr[i] = 0;
		occCb[i] = 0;
	}
	int n = taille[0];
	int m = taille[1];
	for(unsigned int y = 0; y < m; ++y) {
		for(unsigned int x = 0; x < n; ++x) {
			occY[(int)Y[y][x]]++ ;
			occCr[(int)Cr[y][x]]++ ;
			occCb[(int)Cb[y][x]]++ ;
		}
	}

	float freqY[255], freqCr[255], freqCb[255];
	int t = m * n;
	for(unsigned int i = 0; i < 255; i++) {
		freqY[255] = occY[255] / t;
		freqCr[255] = occCr[255] / t;
		freqCb[255] = occCb[255] / t;
	}

	Huffman(freqY, bit, 0);
	Huffman(freqCr, bit, 0);
	Huffman(freqCb, bit, 0);
}

void PSNR(double Y[512][512], double Cr[512][512], double Cb[512][512], double Y1[512][512], double Cr1[512][512], double Cb1[512][512], int* taille, double* distorsion) {
	double eqm[3];
	eqm[0] = eqm[1] = eqm[2] = 0;
	int n = taille[0];
	int m = taille[1];

	for(unsigned int y = 0; y < m; ++y) {
		for(unsigned int x = 0; x < n; ++x) {
			eqm[0] += (Y[y][x] - Y1[y][x]) * (Y[y][x] - Y1[y][x]);
			eqm[1] += (Cr[y][x] - Cr1[y][x]) * (Cr[y][x] - Cr1[y][x]);
			eqm[2] += (Cb[y][x] - Cb1[y][x]) * (Cb[y][x] - Cb1[y][x]);
		}
	}
	eqm[0] = eqm[0] / (m * n);
	eqm[1] = eqm[1] / (m * n);
	eqm[2] = eqm[2] / (m * n);
	int d = 255 * 255;
	distorsion[0] = 10 * log(d / eqm[0]);
	distorsion[1] = 10 * log(d / eqm[1]);
	distorsion[2] = 10 * log(d / eqm[2]);

}

int main(int argc, char* argv[]) {

	int taille[2];

	taille[0] = 512;
	taille[1] = 512;

	double Y[512][512];
	double Cr[512][512];
	double Cb[512][512];

	conversion(Y, Cr, Cb); // Question 1

	double resY[512][512];
	double resCr[512][512];
	double resCb[512][512];

	int N = 1; // 0 < N < 7
	int M = N;

	Tondelette(Y, Cr, Cb, resY, resCr, resCb, taille, N, M);

	float bit[3];
	HuffmanPrep(Y, Cr, Cb, taille, bit);

	double Y1[512][512];
	double Cr1[512][512];
	double Cb1[512][512];
	conversion(Y1, Cr1, Cb1);

	float debit = (bit[0] + bit[1] + bit[2]) / (512 * 512);
	double distorsion[3];
	PSNR(Y, Cr, Cb, Y1, Cr1, Cb1, taille, distorsion);

	return 1;
}
