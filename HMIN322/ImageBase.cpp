/******************************************************************************
* ICAR_Library
*
* Fichier : ImageBase.cpp
*
* Description : Voir le fichier .h
*
* Auteur : Mickael Pinto
*
* Mail : mickael.pinto@live.fr
*
* Date : Octobre 2012
*
*******************************************************************************/

#include "ImageBase.h"
#include "Image_ppm.h"
#include <iostream>



ImageBase::ImageBase(void){
	isValid = false;
	init();
}

ImageBase::ImageBase(int imWidth, int imHeight, bool isColor){
	isValid = false;
	init();

	color = isColor;
	height = imHeight;
	width = imWidth;
	nTaille = height * width * (color ? 3 : 1);

	if(nTaille == 0)
		return;

	allocation_tableau(data, OCTET, nTaille);
	dataD = (double*)malloc(sizeof(double) * nTaille);
	isValid = true;
}


ImageBase::~ImageBase(void){
	reset();
}

void ImageBase::init(){
	if(isValid){
		free(data);
		free(dataD);
	}

	data = 0;
	dataD = 0;
	height = width = nTaille = 0;
	isValid = false;
}

void ImageBase::reset(){
	if(isValid){
		free(data);
		free(dataD);
	}
	isValid = false;
}

void ImageBase::load(char *filename){
	init();

	int l = strlen(filename);

	if(l <= 4) {// Le fichier ne peut pas etre que ".pgm" ou ".ppm"
		printf("Chargement de l'image impossible : Le nom de fichier n'est pas conforme, il doit comporter l'extension, et celle ci ne peut être que '.pgm' ou '.ppm'");
		exit(0);
	}

	int nbPixel = 0;

	if( strcmp(filename + l - 3, "pgm") == 0) { // L'image est en niveau de gris
		color = false;
		lire_nb_lignes_colonnes_image_pgm(filename, &height, &width);
		nbPixel = height * width;

		nTaille = nbPixel;
		allocation_tableau(data, OCTET, nTaille);
		lire_image_pgm(filename, data, nbPixel);
	}
	else if( strcmp(filename + l - 3, "ppm") == 0) { // L'image est en couleur
		color = true;
		lire_nb_lignes_colonnes_image_ppm(filename, &height, &width);
		nbPixel = height * width;

		nTaille = nbPixel * 3;
		allocation_tableau(data, OCTET, nTaille);
		lire_image_ppm(filename, data, nbPixel);
	}
	else{
		printf("Chargement de l'image impossible : Le nom de fichier n'est pas conforme, il doit comporter l'extension, et celle ci ne peut être que .pgm ou .ppm");
		exit(0);
	}

	dataD = (double*)malloc(sizeof(double) * nTaille);

	isValid = true;
}

bool ImageBase::save(char *filename){
	if(!isValid){
		printf("Sauvegarde de l'image impossible : L'image courante n'est pas valide");
		exit(0);
	}

	if(color)
		ecrire_image_ppm(filename, data,  height, width);
	else
		ecrire_image_pgm(filename, data,  height, width);

	return true;
}

ImageBase *ImageBase::getPlan(PLAN plan) {
	if( !isValid || !color )
		return 0;

	ImageBase *greyIm = new ImageBase(width, height, false);

	switch(plan){
	case PLAN_R:
		planR(greyIm->data, data, height * width);
		break;
	case PLAN_G:
		planV(greyIm->data, data, height * width);
		break;
	case PLAN_B:
		planB(greyIm->data, data, height * width);
		break;
	default:
		printf("Il n'y a que 3 plans, les valeurs possibles ne sont donc que 'PLAN_R', 'PLAN_G', et 'PLAN_B'");
		exit(0);
		break;
	}

	return greyIm;
}

void ImageBase::copy(const ImageBase &copy) {
	reset();

	isValid = false;
	init();

	color = copy.color;
	height = copy.height;
	width = copy.width;
	nTaille = copy.nTaille;
	isValid = copy.isValid;

	if(nTaille == 0)
		return;

	allocation_tableau(data, OCTET, nTaille);
	dataD = (double*)malloc(sizeof(double) * nTaille);
	isValid = true;

	for(int i = 0; i < nTaille; ++i)
	{
		data[i] = copy.data[i];
		dataD[i] = copy.dataD[i];
	}

}

unsigned char *ImageBase::operator[](int l) {
	if(!isValid) {
		printf("L'image courante n'est pas valide");
		exit(0);
	}

	if((!color && l >= height) || (color && l >= height*3)) {
		printf("L'indice se trouve en dehors des limites de l'image");
		exit(0);
	}

	return data+l*width;
}

void ImageBase::classification_init(int k, char cNomImgEcrite[250]) {
//k est le nombre de point de référence pour le clustering

  /* int** colors = new int*[k];
  for(int i = 0; i < k; ++i) {
    colors[i] = new int[3];
  }*/

  int** colors = new int*[k];
  for(int i = 0; i < k; ++i) {
    colors[i] = new int[3];
  }

	//Prendre les couleurs dans l'image
	if(k == 2){
		colors[0][0] = 255;
	  colors[0][1] = 0;
	  colors[0][2] = 0;

	  colors[1][0] = 0;
	  colors[1][1] = 255;
	  colors[1][2] = 0;
	}
	else{
			for(int i = 0; i < k; ++i){
				int l, m;
				bool alreadyExists;
				do{
					alreadyExists = false;
					l = rand() % getWidth();
					m = rand() % getHeight();
					for(int j = 0; j < i; ++j) {
						if((*this)[l*3][m]   == colors[j][0] &&
							 (*this)[l*3+1][m] == colors[j][1] &&
						 	 (*this)[l*3+2][m] == colors[j][2]) {
								alreadyExists = true;
						}
					}
				} while(alreadyExists);

			// Choisir un pixel l,m random
			// Vérifier qu'il n'a pas la même couleur que les i - 1 précédents
			// Attribuer la couleur au cluster i si elle est unique
			colors[i][0] = (*this)[l*3][m];
			colors[i][1] = (*this)[l*3+1][m];
			colors[i][2] = (*this)[l*3+2][m];
		}
	}


	//L'initialisation de colors se fera par la suite par la prise
	//de couleurs aléatoires dans l'image

	//return classification(colors, k);
	std::cout<<"Début de classification "<<"\n";
 classification(colors,k,cNomImgEcrite);

}

void ImageBase::copyPlans(ImageBase* red, ImageBase* green, ImageBase* blue) {
  ImageBase* plans[3] = {red, green, blue};
  unsigned int planToUse(0);
  for(int i = 0; i < nTaille; ++i) {
    data[i] = (plans[planToUse])->data[i/3];
    (++planToUse)%=3;
  }
}


void ImageBase::classification(int** colors, int k, char cNomImgEcrite[250]) {

	int changed = 0;

	ImageBase* res_red(getPlan(PLAN_R));
	ImageBase* res_green(getPlan(PLAN_G));
	ImageBase* res_blue(getPlan(PLAN_B));

	ImageBase* red(getPlan(PLAN_R));
	ImageBase* green(getPlan(PLAN_G));
	ImageBase* blue(getPlan(PLAN_B));


	int*** sum = new int**[k];
	for(int i = 0; i < k; ++i) {
		sum[i] = new int*[3];
		for(int j = 0; j < 3; ++j) {
			sum[i][j] = new int[2];
			sum[i][j][0] = 0;
			sum[i][j][1] = 0;
		}

	}

	for(unsigned int i = 0; i < getWidth(); ++i) {
		for(unsigned int j = 0; j < getHeight(); ++j) {
			int closest = 0;
			int min = sqrt(((*red)[i][j]   - colors[0][0]) * ((*red)[i][j]   - colors[0][0]) +
										 ((*green)[i][j] - colors[0][1]) * ((*green)[i][j] - colors[0][1]) +
									 	 ((*blue)[i][j]  - colors[0][2]) * ((*blue)[i][j]  - colors[0][2]));

			for(unsigned int c = 1; c < k; ++c) {
				int tmp = sqrt(((*red)[i][j]   - colors[c][0]) * ((*red)[i][j]   - colors[c][0]) +
											 ((*green)[i][j] - colors[c][1]) * ((*green)[i][j] - colors[c][1]) +
										 	 ((*blue)[i][j]  - colors[c][2]) * ((*blue)[i][j]  - colors[c][2]));
				if(min > tmp) {
					min = tmp;
					closest = c;
				}
			}

			(*res_red)[i][j] = colors[closest][0];
			(*res_green)[i][j] = colors[closest][1];
			(*res_blue)[i][j] = colors[closest][2];

			sum[closest][0][0] += (*red)[i][j];
			sum[closest][1][0] += (*green)[i][j];
			sum[closest][2][0] += (*blue)[i][j];

			sum[closest][0][1]++;
			sum[closest][1][1]++;
			sum[closest][2][1]++;

		}
	}

	for(int i = 0; i < k; ++i) {
		int moy_R = sum[i][0][0] / fmax(sum[i][0][1], 1);
		int moy_G = sum[i][1][0] / fmax(sum[i][1][1], 1);
		int moy_B = sum[i][2][0] / fmax(sum[i][2][1], 1);

		//std::cout<<"R:"<<moy_R<<" G:"<<moy_G<<" B:"<< moy_B<<std::endl;

		changed += fabs(colors[i][0] - moy_R) + fabs(colors[i][1] - moy_G) + fabs(colors[i][2] - moy_B);
		colors[i][0] = moy_R;
		colors[i][1] = moy_G;
		colors[i][2] = moy_B;
	}

	if(changed > 50) {
		std::cout<<"Rappel   ";
		return classification(colors, k, cNomImgEcrite);
	}

	else {
		std::cout<<"Coucou"<<"\n";
		ImageBase result(getWidth(), getHeight(), true);
		result.copyPlans(res_red, res_green, res_blue);
			result.save(cNomImgEcrite);
			std::cout<<"Coucou"<<"\n";

	}

// sqrt((x2−x1)²+(y2−y1)²+(z2−z1)²)
	/* Fonctionnement de k-mean :
		1) On compare chaque pixel de l'image aux pixels représentant les clusters
		2) On classe le pixel dans le cluster le plus proche (la distance se fait avec la distance euclidienne pour les points à 3 coordonnées)
		3) Une fois tous les pixels classés, on calcule la valeur du nouveau réprésentant de chaque cluster sous la forme d'une moyenne des valeurs de ses pixels attribué
		4) Si l'ensemble des valeurs représentantes des clusters ont très peu évoluée, on a atteint l'équilibre, sinon on rappelle cette fonction en remplaçant les valeurs de colors par les nouvelles calculées*/

		// Appliquer l'agorithme de k-mean

		// Comparer la valeur des représentants des clusters

		// Si la variation est faible ou nul, renvoyer l'image en remplaçant les pixels par leurs représentants, sinon rappeler la fonction avec les nouvelles valeurs de colors
}

ImageBase ImageBase::classification2(int** colors, int k, char cNomImgEcrite[250]) {

	int changed = 0;

	ImageBase imRes(getWidth(), getHeight(), true);


	int*** sum = new int**[k];
	for(int i = 0; i < k; ++i) {
		sum[i] = new int*[3];
		for(int j = 0; j < 3; ++j) {
			sum[i][j] = new int[2];
			sum[i][j][0] = 0;
			sum[i][j][1] = 0;
		}

	}

	for(unsigned int i = 0; i < getWidth(); ++i) {
		for(unsigned int j = 0; j < getHeight(); ++j) {
			int closest = 0;
			int min = sqrt(((*this)[i*3][j]   - colors[0][0]) * ((*this)[i*3][j]   - colors[0][0]) +
										 ((*this)[i*3+1][j] - colors[0][1]) * ((*this)[i*3+1][j] - colors[0][1]) +
									 	 ((*this)[i*3+2][j]  - colors[0][2]) * ((*this)[i*3+2][j]  - colors[0][2]));

			for(unsigned int c = 1; c < k; ++c) {
				int tmp = sqrt(((*this)[i*3][j]   - colors[c][0]) * ((*this)[i*3][j]   - colors[c][0]) +
											 ((*this)[i*3+1][j]- colors[c][1]) * ((*this)[i*3+1][j] - colors[c][1]) +
										 	 ((*this)[i*3+2][j]  - colors[c][2]) * ((*this)[i*3+2][j]  - colors[c][2]));
				if(min > tmp) {
					min = tmp;
					closest = c;
				}
			}

			imRes[i*3][j] = colors[closest][0];
			imRes[i*3+1][j] = colors[closest][1];
			imRes[i*3+2][j] = colors[closest][2];

			sum[closest][0][0] += imRes[i*3][j];
			sum[closest][1][0] += imRes[i*3+1][j];
			sum[closest][2][0] += imRes[i*3+2][j];

			sum[closest][0][1]++;
			sum[closest][1][1]++;
			sum[closest][2][1]++;

		}
	}

	for(int i = 0; i < k; ++i) {
		int moy_R = sum[i][0][0] / fmax(sum[i][0][1], 1);
		int moy_G = sum[i][1][0] / fmax(sum[i][1][1], 1);
		int moy_B = sum[i][2][0] / fmax(sum[i][2][1], 1);

		std::cout<<"R:"<<moy_R<<" G:"<<moy_G<<" B:"<< moy_B<<std::endl;

		changed += fabs(colors[i][0] - moy_R) + fabs(colors[i][1] - moy_G) + fabs(colors[i][2] - moy_B);
		colors[i][0] = moy_R;
		colors[i][1] = moy_G;
		colors[i][2] = moy_B;
	}

	if(changed > 50) {
		return classification2(colors, k, cNomImgEcrite);
	}

	else {
		imRes.save(cNomImgEcrite);
		return imRes;
	}
}

ImageBase ImageBase::compress(char outName[250]) {
	ImageBase* red(getPlan(PLAN_R));
	ImageBase* green(getPlan(PLAN_G));
	ImageBase* blue(getPlan(PLAN_B));

	ImageBase redPlan(red->getWidth()/2, red->getHeight()/2, false);
	ImageBase bluePlan(blue->getWidth()/2, blue->getHeight()/2, false);

	ImageBase* res_red(&redPlan);
	ImageBase* res_blue(&bluePlan);

	ImageBase redPlan2(green->getWidth(), green->getHeight(), false);
	ImageBase bluePlan2(green->getWidth(), green->getHeight(), false);

	ImageBase* res_red2(&redPlan2);
	ImageBase* res_blue2(&bluePlan2);

	for(unsigned int i = 0; i < getWidth(); i += 2) {
		for(unsigned int j = 0; j < getHeight(); j += 2) {
			(*res_red)[i/2][j/2] = (*red)[i][j];
			(*res_blue)[i/2][j/2] = (*blue)[i][j];
		}
	}

	printf("Compression faite, décompression...\n");

	for(unsigned int i = 0; i < green->getWidth() - 2; i+=2) {
		for(unsigned int j = 0; j < green->getHeight() - 2; j+=2) {
			(*res_red2)[i][j] = (*res_red)[i/2][j/2];
			(*res_red2)[i+1][j] = (*res_red)[i/2][j/2];
			(*res_red2)[i][j+1] = (*res_red)[i/2][j/2];
			(*res_red2)[i+1][j+1] = (*res_red)[i/2][j/2];

			(*res_blue2)[i][j] = (*res_blue)[i/2][j/2];
			(*res_blue2)[i+1][j] = (*res_blue)[i/2][j/2];
			(*res_blue2)[i][j+1] = (*res_blue)[i/2][j/2];
			(*res_blue2)[i+1][j+1] = (*res_blue)[i/2][j/2];
		}
	}
	ImageBase imRes(getWidth(), getHeight(), true);
	imRes.copyPlans(res_red2, green, res_blue2);
	imRes.save(outName);
	return imRes;
}

void ImageBase::compressYCrCb(char outName[250]) {
	// Converts to YCrCb

	ImageBase* red(getPlan(PLAN_R));
	ImageBase* green(getPlan(PLAN_G));
	ImageBase* blue(getPlan(PLAN_B));
	//  = 0.299 R + 0.587 G + 0.114
	// Cr = (R – Y) / (2 – 2 * 0.299 R) + 128
	// Cb =  (B – Y) / (2 – 2* 0.114 B) + 128
	ImageBase Y(getWidth(), getHeight(), false);
	ImageBase Cr(getWidth(), getHeight(), false);
	ImageBase Cb(getWidth(), getHeight(), false);

	for(unsigned int i = 0; i < getWidth(); ++i) {
		for(unsigned int j = 0; j < getHeight(); ++j) {
			Y[i][j] = 0.299 * (*red)[i][j] + 0.587 * (*green)[i][j] + 0.114 * (*blue)[i][j];
			Cr[i][j] = 128 + (0.5 * (*red)[i][j]) - (0.418688 * (*green)[i][j]) - (0.081312 * (*blue)[i][j]);
			Cb[i][j] = 128 - (0.168736 * (*red)[i][j]) - (0.331264 * (*green)[i][j]) + (0.5 * (*blue)[i][j]);
		}
	}

	ImageBase resCr(getWidth()/2, getHeight()/2, false);
	ImageBase resCb(getWidth()/2, getHeight()/2, false);

	for(unsigned int i = 0; i < getWidth(); i+=2) {
		for(unsigned int j = 0; j < getHeight(); j+=2) {
			resCr[i/2][j/2] = Cr[i][j];
			resCb[i/2][j/2] = Cb[i][j];
		}
	}

	for(unsigned int i = 0; i < getWidth(); i+=2) {
		for(unsigned int j = 0; j < getHeight(); j+=2) {
			Cr[i][j] = resCr[i/2][j/2];
			Cr[i+1][j] = resCr[i/2][j/2];
			Cr[i][j+1] = resCr[i/2][j/2];
			Cr[i+1][j+1] = resCr[i/2][j/2];

			Cb[i][j] = resCb[i/2][j/2];
			Cb[i+1][j] = resCb[i/2][j/2];
			Cb[i][j+1] = resCb[i/2][j/2];
			Cb[i+1][j+1] = resCb[i/2][j/2];
		}
	}

	ImageBase resRed(getWidth(), getHeight(), false);
	ImageBase resGreen(getWidth(), getHeight(), false);
	ImageBase resBlue(getWidth(), getHeight(), false);

	for(unsigned int i = 0; i < getWidth(); ++i) {
		for(unsigned int j = 0; j < getHeight(); ++j) {
			resRed[i][j] = Y[i][j] + 1.402 * (Cr[i][j] - 128);
			resGreen[i][j] = Y[i][j] - 0.344136 * (Cb[i][j] - 128) - 0.714136 * (Cr[i][j] - 128);
			resBlue[i][j] = Y[i][j] + 1.772 * (Cb[i][j] - 128);
		}
	}

	ImageBase *resultRed(&resRed);
	ImageBase *resultGreen(&resGreen);
	ImageBase *resultBlue(&resBlue);

	ImageBase imRes(getWidth(), getHeight(), true);
	imRes.copyPlans(resultRed, resultGreen, resultBlue);
	imRes.save(outName);

}
