#include "ImageBase.h"
#include <stdio.h>
#include <iostream>
#include <cmath>

int main(int argc, char **argv)
{
	///////////////////////////////////////// Exemple d'un seuillage d'image
	char cNomImgLue[250]; //cNomImgEcrite[250];
	char imgOut[250];
	int S;

	if (argc != 3)
	{
		printf("Usage: ImageIn.pgm ImageOut.pgm \n");
		return 1;
	}
	sscanf (argv[1],"%s",cNomImgLue) ;
	sscanf (argv[2],"%s",imgOut);


	//ImageBase imIn, imOut;
	ImageBase imIn;
	imIn.load(cNomImgLue);

	//imIn.compressYCrCb(imgOut);
	ImageBase imIn2;
	imIn2.load(imgOut);

	double psnr, eqm = 0;
	for(unsigned int i = 0; i < imIn.getWidth() * 3; i++) {
		for(unsigned int j = 0; j < imIn.getHeight(); ++j) {
			eqm += imIn[i][j] - imIn2[i][j];
		}
	}
	eqm /= 3.0 * imIn.getWidth() * imIn.getHeight();
	psnr = 20.0 * log10(sqrt((255 * 255) * 3) / eqm);
	std::cout<<"EQM : "<< eqm << ", PSNR : " << psnr << std::endl;

  //imIn.classification_init(256, cNomImgEcrite);
	//imOut.save(cNomImgEcrite);


	//ImageBase imG(imIn.getWidth(), imIn.getHeight(), imIn.getColor());
	/*ImageBase imOut(imIn.getWidth(), imIn.getHeight(), imIn.getColor());

	for(int x = 0; x < imIn.getHeight(); ++x)
		for(int y = 0; y < imIn.getWidth(); ++y)
		{
			if (imIn[x][y] < S)
				imOut[x][y] = 0;
			else imOut[x][y] = 255;
		}

	imOut.save(cNomImgEcrite);




	///////////////////////////////////////// Exemple de création d'une image couleur
	ImageBase imC(50, 100, true);

	for(int y = 0; y < imC.getHeight(); ++y)
		for(int x = 0; x < imC.getWidth(); ++x)
		{
			imC[y*3][x*3+0] = 200; // R
			imC[y*3][x*3+1] = 0; // G
			imC[y*3][x*3+2] = 0; // B
		}

	imC.save("imC.ppm");




	///////////////////////////////////////// Exemple de création d'une image en niveau de gris
	ImageBase imG(50, 100, false);

	for(int y = 0; y < imG.getHeight(); ++y)
		for(int x = 0; x < imG.getWidth(); ++x)
			imG[y][x] = 50;

	imG.save("imG.pgm");




	ImageBase imC2, imG2;

	///////////////////////////////////////// Exemple lecture image couleur
	imC2.load("imC.ppm");
	///////////////////////////////////////// Exemple lecture image en niveau de gris
	imG2.load("imG.pgm");



	///////////////////////////////////////// Exemple de récupération d'un plan de l'image
	ImageBase *R = imC2.getPlan(ImageBase::PLAN_R);
	R->save("R.pgm");
	delete R;*/



	return 0;
}
