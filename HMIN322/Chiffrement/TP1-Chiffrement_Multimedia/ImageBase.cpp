/******************************************************************************
* ICAR_Library
*
* Fichier : ImageBase.cpp
*
* Description : Voir le fichier .h
*
* Auteur : Mickael Pinto
*
* Mail : mickael.pinto@live.fr
*
* Date : Octobre 2012
*
*******************************************************************************/

#include "ImageBase.h"
#include "Image_ppm.h"
#include <iostream>
#include <random>
#include <cmath>


ImageBase::ImageBase(void){
	isValid = false;
	init();
}

ImageBase::ImageBase(int imWidth, int imHeight, bool isColor){
	isValid = false;
	init();

	color = isColor;
	height = imHeight;
	width = imWidth;
	nTaille = height * width * (color ? 3 : 1);

	if(nTaille == 0)
		return;

	allocation_tableau(data, OCTET, nTaille);
	dataD = (double*)malloc(sizeof(double) * nTaille);
	isValid = true;
}


ImageBase::~ImageBase(void){
	reset();
}

void ImageBase::init(){
	if(isValid){
		free(data);
		free(dataD);
	}

	data = 0;
	dataD = 0;
	height = width = nTaille = 0;
	isValid = false;
}

void ImageBase::reset(){
	if(isValid){
		free(data);
		free(dataD);
	}
	isValid = false;
}

void ImageBase::load(char *filename){
	init();

	int l = strlen(filename);

	if(l <= 4) {// Le fichier ne peut pas etre que ".pgm" ou ".ppm"
		printf("Chargement de l'image impossible : Le nom de fichier n'est pas conforme, il doit comporter l'extension, et celle ci ne peut être que '.pgm' ou '.ppm'");
		exit(0);
	}

	int nbPixel = 0;

	if( strcmp(filename + l - 3, "pgm") == 0) { // L'image est en niveau de gris
		color = false;
		lire_nb_lignes_colonnes_image_pgm(filename, &height, &width);
		nbPixel = height * width;

		nTaille = nbPixel;
		allocation_tableau(data, OCTET, nTaille);
		lire_image_pgm(filename, data, nbPixel);
	}
	else if( strcmp(filename + l - 3, "ppm") == 0) { // L'image est en couleur
		color = true;
		lire_nb_lignes_colonnes_image_ppm(filename, &height, &width);
		nbPixel = height * width;

		nTaille = nbPixel * 3;
		allocation_tableau(data, OCTET, nTaille);
		lire_image_ppm(filename, data, nbPixel);
	}
	else{
		printf("Chargement de l'image impossible : Le nom de fichier n'est pas conforme, il doit comporter l'extension, et celle ci ne peut être que .pgm ou .ppm");
		exit(0);
	}

	dataD = (double*)malloc(sizeof(double) * nTaille);

	isValid = true;
}

bool ImageBase::save(char *filename){
	if(!isValid){
		printf("Sauvegarde de l'image impossible : L'image courante n'est pas valide");
		exit(0);
	}

	if(color)
		ecrire_image_ppm(filename, data,  height, width);
	else
		ecrire_image_pgm(filename, data,  height, width);

	return true;
}

ImageBase *ImageBase::getPlan(PLAN plan) {
	if( !isValid || !color )
		return 0;

	ImageBase *greyIm = new ImageBase(width, height, false);

	switch(plan){
	case PLAN_R:
		planR(greyIm->data, data, height * width);
		break;
	case PLAN_G:
		planV(greyIm->data, data, height * width);
		break;
	case PLAN_B:
		planB(greyIm->data, data, height * width);
		break;
	default:
		printf("Il n'y a que 3 plans, les valeurs possibles ne sont donc que 'PLAN_R', 'PLAN_G', et 'PLAN_B'");
		exit(0);
		break;
	}

	return greyIm;
}

void ImageBase::copy(const ImageBase &copy) {
	reset();

	isValid = false;
	init();

	color = copy.color;
	height = copy.height;
	width = copy.width;
	nTaille = copy.nTaille;
	isValid = copy.isValid;

	if(nTaille == 0)
		return;

	allocation_tableau(data, OCTET, nTaille);
	dataD = (double*)malloc(sizeof(double) * nTaille);
	isValid = true;

	for(int i = 0; i < nTaille; ++i)
	{
		data[i] = copy.data[i];
		dataD[i] = copy.dataD[i];
	}

}

unsigned char *ImageBase::operator[](int l) {
	if(!isValid) {
		printf("L'image courante n'est pas valide");
		exit(0);
	}

	if((!color && l >= height) || (color && l >= height*3)) {
		printf("L'indice se trouve en dehors des limites de l'image");
		exit(0);
	}

	return data+l*width;
}


void ImageBase::copyPlans(ImageBase* red, ImageBase* green, ImageBase* blue) {
  ImageBase* plans[3] = {red, green, blue};
  unsigned int planToUse(0);
  for(int i = 0; i < nTaille; ++i) {
    data[i] = (plans[planToUse])->data[i/3];
    (++planToUse)%=3;
  }
}

std::array<unsigned int, 256> ImageBase::getGrayScaleHistogram() {
  std::array<unsigned int, 256> occurrences = {0};
  for (unsigned int y = 0; y < getHeight(); ++y) {
    for (unsigned int x = 0; x < getWidth(); ++x) {
      ++occurrences[(*this)[y][x]];
    }
  }
  return occurrences;
}


std::vector<unsigned char> ImageBase::sequence(int key) {
	std::vector<unsigned char> result;
	std::srand(key);
	for(unsigned int i=0; i < nTaille; ++i) {
		result.push_back(static_cast<unsigned char>(std::rand() % 256));
	}

	return result;
}

void ImageBase::chiffrement(int key,  char outName[250]) {
	ImageBase res(getWidth(), getHeight(), false);
	std::vector<unsigned char> seq = sequence(key);
	for(int i = 0; i < nTaille; ++i) {
		res.data[i] = data[i] ^ seq[i];
	}

	res.save(outName);
}

void ImageBase::chiffrement(int key, ImageBase *res) {
	std::vector<unsigned char> seq = sequence(key);
	for(int i = 0; i < nTaille; ++i) {
		res->data[i] = data[i] ^ seq[i];
	}
}

double ImageBase::getEntropy() {
	std::array<unsigned int, 256> histo(getGrayScaleHistogram());
	double entropy = 0.0;
	for(unsigned int val : histo) {
		if(val != 0) {
			double proba = static_cast<double>(val) / static_cast<double>(nTaille);
			entropy += proba * std::log2(proba);
		}
	}

	return (0 - entropy);
}

void ImageBase::attack() {
	double min_entropy = 8.0;
	int key = 0;
	ImageBase* res = new ImageBase(getWidth(), getHeight(), false);
	for(unsigned int i = 0; i < 100; ++i) {
		chiffrement(i, res);
		double entro = res->getEntropy();
		if(entro < min_entropy) {
			min_entropy = entro;
			key = i;
		}
	}
	std::cout<<"Key :"<< key<< std::endl;

	chiffrement(key, "res_attack_zeta.pgm");
}
