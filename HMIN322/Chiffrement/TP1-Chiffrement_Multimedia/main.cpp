#include "ImageBase.h"
#include <stdio.h>
#include <iostream>
#include <cmath>

int main(int argc, char **argv)
{
	///////////////////////////////////////// Exemple d'un seuillage d'image
	char cNomImgLue[250]; //cNomImgEcrite[250];
	char imgOut[250];
	int S;

	if (argc != 3)
	{
		printf("Usage: ImageIn.pgm ImageOut.pgm \n");
		return 1;
	}
	sscanf (argv[1],"%s",cNomImgLue) ;
	sscanf (argv[2],"%s",imgOut);

	ImageBase imIn;
	imIn.load(cNomImgLue);

	// imIn.chiffrement(27, imgOut);

	imIn.attack();


	return 0;
}
