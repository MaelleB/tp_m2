#include <igl/opengl/glfw/Viewer.h>
#include <igl/readOFF.h>
#include <igl/read_triangle_mesh.h>
#include <igl/write_triangle_mesh.h>

#include <iostream>

#include "Mesh.h"

int main(int argc, char *argv[]) {
  // Inline mesh of a cube
  Eigen::MatrixXd V;
  Eigen::MatrixXi F;

  igl::read_triangle_mesh("../tp/meshes/bunny.off", V, F);
  igl::write_triangle_mesh("../tp/meshes/bunny_copy.off", V, F);
  
  Mesh bunny(V, F);
  Eigen::Vector3d barycenter = bunny.barycenter();
  std::cout << barycenter[0] << "," << barycenter[1] << "," << barycenter[2] << std::endl;
  // Plot the mesh
  igl::opengl::glfw::Viewer viewer;

  bunny.updateViewer(viewer);
  viewer.launch();

}
