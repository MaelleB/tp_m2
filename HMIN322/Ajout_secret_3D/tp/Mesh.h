#ifndef MESH_H
#define MESH_H

#include <igl/opengl/glfw/Viewer.h>
#include <utility>

class Mesh {

private:
  Eigen::MatrixXd V;
  Eigen::MatrixXi F;

  Eigen::MatrixXd VSphere;
public:
  Mesh(const Eigen::MatrixXd& v, const Eigen::MatrixXi& f);

  void updateViewer(igl::opengl::glfw::Viewer& viewer) const;

  Eigen::Vector3d barycenter() const;
  void sphericalCoord();
  void cartesianCoord();

  std::pair<double, double> minMax(unsigned int begin, unsigned int end) const;

  void normalize(unsigned int begin, unsigned int end);
  void unnormalize(unsigned int begin, unsigned int end, std::pair<double, double> min_max);
  void sort();
};

#endif /* end of include guard: MESH_H */
