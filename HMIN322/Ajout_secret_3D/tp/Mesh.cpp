#include <cmath>
#include <limits>
#include <algorithm>

#include "Mesh.h"


Mesh::Mesh(const Eigen::MatrixXd& v, const Eigen::MatrixXi& f) :
  V(v), F(f) {
    VSphere.resize(v.rows(), v.cols());
  }

void Mesh::updateViewer(igl::opengl::glfw::Viewer& viewer) const {
  viewer.data().set_mesh(V, F);
  viewer.data().set_face_based(true);
}

Eigen::Vector3d Mesh::barycenter() const {
  Eigen::Vector3d result(0, 0, 0);

  for(unsigned int i = 0; i < V.size(); i += 3) {
    result[0] += V(i);
    result[1] += V(i+1);
    result[2] += V(i+2);
  }
  result /= V.size()/3;

  return result;
}

void Mesh::sphericalCoord() {
  Eigen::Vector3d bary = barycenter();
  double rho, theta, phi;
  for(unsigned int i = 0; i < V.size(); i += 3) {

    // distance
    rho = sqrt((V(i) - bary[0]) * (V(i) - bary[0]) +
                     (V(i+1) - bary[1]) * (V(i+1) - bary[1]) +
                     (V(i+2) - bary[2]) * (V(i+2) - bary[2]));
    theta = V(i) == bary[0] ? 0 : atan2(V(i+1) - bary[1], V(i) - bary[0]);
    phi = rho == 0 ? 0 : acos((V(i+2) - bary[2]) / rho);

    VSphere(i) = rho;
    VSphere(i+1) = theta;
    VSphere(i+2) = phi;
  }
}

void Mesh::cartesianCoord() {
  Eigen::Vector3d bary = barycenter();
  double x, y, z;

  for(unsigned int i = 0; i < VSphere.size(); i += 3) {
    x = VSphere(i) * cos(VSphere(i+1)) * sin(VSphere(i+2)) + bary[0];
    y = VSphere(i) * sin(VSphere(i+1)) * sin(VSphere(i+2)) + bary[1];
    z = VSphere(i) * cos(VSphere(i+2)) + bary[2];

    V(i) = x;
    V(i+1) = y;
    V(i+2) = z;
  }
}

std::pair<double, double> Mesh::minMax(unsigned int begin, unsigned int end) const {
  std::pair<double, double> result = {std::numeric_limits<double>::max(),
                                      std::numeric_limits<double>::min()};
  for(unsigned int i=begin; i < end; i += 3) {
    if(VSphere(i) < result.first) {
      result.first = VSphere(i);
    }
    if(VSphere(i) > result.second) {
      result.second = VSphere(i);
    }
  }
}

void Mesh::normalize(unsigned int begin, unsigned int end) {
  std::pair<double, double> min_max = minMax(begin, end);
  double min = min_max.first;
  double max = min_max.second;

  for(unsigned int i = begin; i < end; i += 3) {
    // -min / (max - min)
    VSphere(i) = (VSphere(i) - min) / (max - min);
  }
}

void Mesh::unnormalize(unsigned int begin, unsigned int end, std::pair<double, double> min_max) {
  double min = min_max.first;
  double max = min_max.second;

  for(unsigned int i = begin; i < end; i += 3) {
    VSphere(i) = VSphere(i) * (max - min) + min;
  }
}

void Mesh::sort() {

}
