# Son et musique

### Gammes

##### Mode majeur

--- tonique + 1 octave (x2)

--- septième

--

--- sixte

--

--- quinte (dominante) (x3/2)

--

--- quarte (x4/3)

--- tierce majeure (x5/4)

--

--- seconde

--

--- tonique

On ne joue que les notes en ---, pas celles en -- .


 ##### Mode mineur

 Même chose que majeure mais avec tierce mineure (1 en dessous), sixte et septième un en dessous.
